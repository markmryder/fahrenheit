package sheridan;

public class Fahrenheit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try 
		{
			System.out.println(5+" in Celsius is: "+convertFromCelsius(5)+" Fahrenheit");
		}
		
		catch(Exception e) {}
	}
	
	public static int convertFromCelsius(int celsius) throws Exception
	{

		if(celsius < -273) {
			throw new Exception("Out of range - below absolute zero");
		}
		int fahrenheit = (int) Math.round((1.8 * celsius) + 32);
		return fahrenheit;
	}

}
