package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFahrenheitAcceptableNumber() throws Exception
	{
		int f = Fahrenheit.convertFromCelsius(100);
		System.out.println(f);
		assertTrue("Invalid fahrenheit", f == 212);
	}
	@Test(expected = Exception.class)
	public void testFahrenheitUnacceptableNumber() throws Exception
	{
		int f = Fahrenheit.convertFromCelsius(-300);
	}
	
	@Test
	public void testFahrenheitBoundaryIn() throws Exception
	{
		int f = Fahrenheit.convertFromCelsius(-273);
		System.out.println(f);
		assertTrue("Invalid celsius", f == -459);
	}
	
	@Test(expected = Exception.class)
	public void testFahrenheitBoundaryOut() throws Exception
	{
		int f = Fahrenheit.convertFromCelsius(-274);
	}

	

}
